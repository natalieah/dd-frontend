import React from 'react';
import Start from './components/Start.jsx';
import Intro from './components/Intro.jsx';
import BackBtn from './components/BackBtn.jsx';
import Routes from './components/routes.js';


export default () => (
  <Routes />
);
